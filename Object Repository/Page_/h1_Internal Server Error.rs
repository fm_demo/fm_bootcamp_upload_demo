<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_Internal Server Error</name>
   <tag></tag>
   <elementGuidId>859dfb7f-7989-462a-9423-a49fc2c712fa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='/html[1]/body[1]/h1[1]'])[1]/preceding::h1[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>37b3c851-5f56-4994-aa8c-5df741fe541e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Internal Server Error</value>
      <webElementGuid>c4534257-3c04-4bf6-8419-e73a4f6146b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/h1[1]</value>
      <webElementGuid>c07f4bf9-8871-4997-af1c-ac45373c4ade</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='/html[1]/body[1]/h1[1]'])[1]/preceding::h1[1]</value>
      <webElementGuid>c99f4a9d-0dce-4435-97ae-3ca4dd96fc4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Internal Server Error']/parent::*</value>
      <webElementGuid>e7561661-078d-4526-8f2b-6a7bff1312d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>346edd7c-7db0-423d-858a-5555eca39f3d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Internal Server Error' or . = 'Internal Server Error')]</value>
      <webElementGuid>97f11d39-0c6b-4b72-abe9-ac3da079b86b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
